fetch(){
  path="$1-$ver.src"
  base="$path.tar.xz"
  file="$sources/$base"
  dest="wasi-sdk/src/llvm-project"
  if [[ ! -f "$file" ]]; then
    curl  -L "$url$ver/$base" -o "$file"
  fi
  mkdir -p "$dest"
  (
  cd "$dest"
  rm -Rfv "$1" "$path"
  if [[ -n "$2" ]]
  then
  b=()
  for i in "${@:2}"
  do
    case "$i" in
    --* )
      b+=("$i") ;;
    * )
      b+=("*/$i") ;;
    esac
  done
  tar --wildcards -xJf "$file" "${b[@]}"
  else
  tar -xJf "$file"
  fi
  mv -Tv "$path" "$1"
  )
}

built(){
  touch "build/$1.BUILT"
}

check_command(){
  for i in "${@}"
  do
    pkgr="$(command -v "${i}")"
    if [[ "${pkgr}" ]]
    then
      export pkgr
      return 0
    fi
  done
  return 1
}

if [[ -n "${HGALLOW}" ]]
then

git clone --single-branch --depth 1 https://github.com/WebAssembly/wasi-sdk

#ver="${ver:-$(zypper info llvm18-devel  | sed -n 's~Version[[:space:]]*: \([^-]*\)-.*~\1~p')}"

sources="${sources:-$(pwd)}"
url='https://github.com/llvm/llvm-project/releases/download/llvmorg-'


#commit="$(git rev-parse HEAD)"
#mtime="$(git show -s --format=%ci "$commit")"
if [[ ! "$ver" ]]
then
if check_command dnf dnf5
then
ver="$($pkgr -q repoquery --qf '%{version}' llvm-devel)"
elif check_command zypper
then
ver="$($pkgr info llvm-devel  | sed -n 's~Version[[:space:]]*: \([^-]*\)-.*~\1~p')"
fi
fi

fetch runtimes
fetch libcxxabi --exclude test
fetch libcxx --exclude test --exclude benchmarks
fetch llvm cmake/config.guess utils

pushd wasi-sdk

rm -Rfv .git

mkdir -p "${dir}"
submod='src/wasi-libc'
rm -R "${submod}"
git clone --single-branch --depth 1 https://github.com/trcrsired/wasi-libc.git "${submod}"
rm -Rf "${submod}/.git"
echo "${ver}" > VERSION

F='~FALSE~'
for i in src/llvm-project/libcxx{,abi}/CMakeLists.txt
do
  sed -i "/if/{s~LIBCXX_INCLUDE_TESTS${F};s~LIBCXXABI_INCLUDE_TESTS${F};s~LIBCXX_INCLUDE_BENCHMARKS${F}}" "$i"
done
popd
else
ver="$(cat VERSION)"
fi

if [[ "${HGALLOW}" == 'archive' ]]
then
mv wasi-sdk "wasi-sdk-$ver"
tar -cJf "wasi-sdk-$ver"{.tar.xz,}

elif [[ -n "${HGURL}" ]]
then
hg clone --noupdate "${HGURL}" shallow-repo
pushd shallow-repo
mv ../wasi-sdk ./
hg add .
hg commit -m update
hg tag "$ver"
hg push -f --config paths.default="${HGURL}" --config auth.default.prefix="${HGURL}" --config auth.default.username="${HGUSER}" --config auth.default.password="${HGPASSWORD}"
popd
else

if [[ -n "${HGALLOW}" ]]
then
cd wasi-sdk
fi

export PREFIX=/opt/wasi-sdk
dir="${PWD}/build/install/${PREFIX}"

bindir="${bindir:-/bin}"
libdir="${libdir:-/usr/lib64}/cmake/llvm"
datadir="${datadir:-/usr/share}/cmake"

mkdir -p "${dir}"
built llvm
built wasm-component-ld
built compiler-rt
#built wasi-libc
ln -sTfv "${bindir}" "${dir}/bin"

d="$PWD/src/llvm-project/llvm/cmake"

mkdir -p "$d"
ln -sTfv "${libdir}" "$d/modules"
ln -sTfv "${datadir}" "$d/../../cmake"
mkdir -p "${PWD}/build/llvm/lib/clang"

make build/libcxx.BUILT

fi
