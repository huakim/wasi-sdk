Name: wasi-sdk
Version: 18.1.1
Provides: wasi-libcxx
Provides: wasi-libc
Provides: wasi-libcxxabi
Provides: wasi-libc++
Provides: wasi-libc++abi
Summary: WASI-enabled WebAssembly C/C++ toolchain
License: GPLv3
Source0: %{name}-%{version}.tar.xz
Source1: main.sh
Release: 0
BuildArch: noarch
BuildRequires: llvm-devel
BuildRequires: clang
BuildRequires: llvm-cmake-utils
BuildRequires: lld
BuildRequires: wasm-component-ld
BuildRequires: wasi-compiler-rt
BuildRequires: (libc++-devel or libcxx-devel)
BuildRequires: (libc++abi-devel or libcxxabi-devel)
BuildRequires: llvm-cmake-host_triple
BuildRequires: cmake
BuildRequires: make
BuildRequires: (ninja or ninja-build)


%description
%{summary}.

%define wasi_sysroot %{_datadir}/wasi-sysroot
%define macro_file mkdir -p %{buildroot}%{_rpmmacrodir}; echo %%wasi_sysroot %{wasi-sysroot} > %{buildroot}%{_rpmmacrodir}/wasi_sysroot.macros
%package macros
Summary: Macros for wasi
BuildArch: noarch
%description macros
%{summary}.

%files macros
%{_rpmmacrodir}/wasi_sysroot.macros

%prep
%autosetup -p1 -n %{name}-%{version}

%build
cp "%{SOURCE1}" main.sh
export bindir='%_bindir'
export libdir='%_libdir'
export datadir='%_datadir'
bash -x main.sh

%install
%macro_file
rm -R '%{buildroot}%{wasi_sysroot}' ||:
mkdir "$(dirname %{buildroot}%{wasi_sysroot})" ||:
mv -Tv 'build/install/opt/wasi-sdk/share/wasi-sysroot' '%{buildroot}%{wasi_sysroot}'
rm -R '%{buildroot}%{wasi_sysroot}'/*/*/*.so

%files
%dir %{wasi_sysroot}
%{wasi_sysroot}/*



